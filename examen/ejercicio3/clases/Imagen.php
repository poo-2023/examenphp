<?php 

namespace clases;
class Imagen{

    // atributos de clase
    private string $src="";
    private bool $border = false;
    private int $ancho = 0;
    private int $alto = 0;

    // constante que indica la ruta donde estan las imagenes
    const ruta = "../imgs/";

    // constructor de la clase
    public function __construct(string $src, int $ancho = null, int $alto = null, bool $border = false){
        $this->src = $src;
        $this->ancho = $ancho;
        $this->alto = $alto;
        $this->border = $border;
    }

    // getters y setters 
    public function getSrc(){
        return $this->src;
    }

    public function setSrc($src){
        $this->src = $src;
    }

    public function getBorder(){
        return $this->border;
    }

    public function setBorder($border){
        $this->border = $border;
    }

    public function getAncho(){
        return $this->ancho;
    }

    public function setAncho($ancho){
        $this->ancho = $ancho;
    }

    public function getAlto(){
        return $this->alto;
    }

    public function setAlto($alto){
        $this->alto = $alto;
    }

    public function __toString(){
        $salida = "";

        $salida = "<img src=\"".self::ruta.$this->src."\" width=\"".$this->ancho."\" height=\"".$this->alto."\" border=\"".$this->border."\" />";


        return $salida;
    }   
        
}
