<?php

use clases\Imagen;

spl_autoload_register(function($clase){
    include "{$clase}.php";
});

// instancia de objetos de la clase imagen
$imagen1 = new Imagen("foto1.jpg", 100, 100);
$imagen2 = new Imagen("foto2.jpg", 100, 100);
$imagen3 = new Imagen("foto3.jpg", 100, 100);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <h1>Imágenes</h1>
    <div>
        <?= $imagen1 ?>
        <?= $imagen2 ?>
        <?= $imagen3 ?>
    </div>

    <div>

        <?php
            echo "<div>";
            $imagen1->setAncho(200);
            $imagen1->setAlto(230);
            $imagen1->setBorder(true);

            $imagen2->setAncho(400);
            $imagen2->setAlto(500);
            $imagen2->setBorder(true);

            $imagen3->setAncho(600);
            $imagen3->setAlto(720);
            $imagen3->setBorder(true);

            echo $imagen1;
            echo $imagen2;
            echo $imagen3;
            
            echo "</div>";
        ?>
    </div>
</body>
</html>

