<?php 

namespace clases;
class Articulo{

    // atributos
    protected ?string $nombre = null;
    protected float $precio = 0.0;

    // cosntructor
    public function __construct(string $nombre, float $precio){
        $this->nombre = $nombre;
        $this->precio = $precio;
    }

    // toString
    public function __toString(){
        return "Nombre: " . $this->nombre . " - " . $this->precio. "€<br>";
    }

    // getters y setters
    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre(string $nombre){
        $this->nombre = $nombre;
    }

    public function getPrecio(){
        return $this->precio;
    }

    public function setPrecio(float $pprecio){
        if(gettype($pprecio) == "float"){
            $this->precio = $pprecio;
        }
    }

    // metodo get de la clase Articulo
    public function getPrecioArticulo()
    {
        return Articulo::getPrecio();
    }
    

}