<?php 

namespace clases;

final class ArticuloRebajado extends Articulo{

    // atributos
    private float $rebaja = 0.0;

    // constructor
    public function __construct(string $pnombre, float $pprecio, float $prebaja){
        parent::__construct($pnombre, $pprecio);
        $this->rebaja = $prebaja;
    }

    // metodo privado para calcular descuento
    private function calcularDescuento()
    {
        return ($this->getPrecio() * $this->rebaja) / 100;
    }

    // metodo public que devuelve el precio menos el descuento
    public function precioRebajado()
    {
        return $this->getPrecio() - $this->calcularDescuento();
    }

    // toString
    public function __toString(){
        return parent::__toString() . "La rebaja es: " . $this->rebaja . "% y el descuento es " . $this->calcularDescuento() . "€<br>";
    }

}