<?php

use clases\ArticuloRebajado;
use clases\Articulo;

spl_autoload_register(function($clase){
    include "{$clase}.php";
});

$articulo1 = new Articulo("Articulo 1", 10.0);
$articulo2 = new Articulo("Articulo 2", 20.0);
$articuloRebajado1 = new ArticuloRebajado("Articulo rebajado 1", 30.0, 10.0);
$articuloRebajado2 = new ArticuloRebajado("Articulo rebajado 2", 40.0, 20.0);

?>  

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?= $articulo1->__toString()?>
    <?= $articulo2->__toString()?>
    <?= $articuloRebajado1->__toString()?>
    <?= $articuloRebajado2->__toString()?>
</body>
</html>