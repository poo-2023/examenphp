<?php

use clases\Imagen;

spl_autoload_register(function($clase){
    include "{$clase}.php";
});

// instancia de objetos
$imagen1 = new Imagen("foto1.jpg", 100, 100);
$imagen2 = new Imagen("foto2.jpg", 100, 100);
$imagen3 = new Imagen("foto3.jpg", 100, 100);

// mostramos todos los valores de los objteos
var_dump($imagen1);
var_dump($imagen2);
var_dump($imagen3);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <h1>Imágenes</h1>
    <div>
        <?= $imagen1 ?>
        <?= $imagen2 ?>
        <?= $imagen3 ?>  
    </div>
    
</body>
</html>