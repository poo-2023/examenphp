<?php

use clases\Imagen;

spl_autoload_register(function($clase){
    include "{$clase}.php";
});

// imagen con la ruta incorrecta
// $imagen1 = new Imagen("fotos1.jpg", 100, 100);

// imagen con el borde incorrecto
// $imagen2 = new Imagen("foto2.jpg", 100, 100, -123);

// imagen correcta
$imagen3 = new Imagen("foto3.jpg", 100, 100, 2);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<h1>Imágenes</h1>

<div>
    <!-- imagen incorrecta -->
    <?= $imagen1 ?>

    <!-- imagen incorrecta -->
    <?= $imagen2 ?>

    <!-- imagen correcta -->
    <?= $imagen3 ?>
</div>
</body>
</html>