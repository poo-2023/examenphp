<?php 

namespace clases;
class Imagen{

    // atributos de clase
    private string $src="";
    private bool $border = false;
    private int $ancho = 0;
    private int $alto = 0;

    // constante que indica la ruta donde estan las imagenes
    const ruta = "../imgs/";

    // constructor de la clase
    public function __construct(string $psrc, int $ancho = null, int $alto = null, int $pborder = 0){
        $this->setSrc($psrc);
        $this->ancho = $ancho;
        $this->alto = $alto;
        $this->setBorder($pborder);
    }

    // getters y setters 
    public function getSrc(){
        return $this->src;
    }

    public function setSrc($ruta){
        if(file_exists(self::ruta.$ruta)){
            $this->src = $ruta;
        }else{
            exit ("No existe la imagen");
        }
    }

    public function getBorder(){
        return $this->border;
    }

    public function setBorder($pBorder){
        if($pBorder>=0){
            $this->border = $pBorder;
        }else{
            exit("El valor debe ser mayor o igual que 0");
        }
    }

    public function getAncho(){
        return $this->ancho;
    }

    public function setAncho($ancho){
        $this->ancho = $ancho;
    }

    public function getAlto(){
        return $this->alto;
    }

    public function setAlto($alto){
        $this->alto = $alto;
    }

    public function __toString(){
        $salida = "";

        $salida = "<img src=\"".self::ruta.$this->src."\" width=\"".$this->ancho."\" height=\"".$this->alto."\" border=\"".$this->border."\" />";


        return $salida;
    }   
        
}
